# Opti-Project

## Introduction  

Here you can find Opti-Project docs


## Prerequisites  

- install `nodejs` & `npm`
- create an Atlas account

## create an Atlas account
- Go to this Url : https://cloud.mongodb.com/user#/atlas/login & create an account
- When you're logged, go to create a new cluster.
- In you cluster's dashboard page  click on `Network Acess` add click `Allow access from anywhere` .
- Configure Connect cluster with “Allow access from anywhere”
- Connect your Application and choisir la version du driver
- Pour créer les endpoints de nos différentes routes de notre application pour la logique

## How to use it with Atlas

Clone this repository to your local storage:
`git clone https://eamarelo@bitbucket.org/eamarelo/opti-project.git`
<br>
and install dependencies:
`npm i`
<br>
if you want to run the project to http://localhost:3000/  with Atlas:
`npm start`



## How to use it in local

### Before to run in local

You have to create your mongodb replicaSet.
You need to create 3 instances of mongo.


 run this cli : `sudo mkdir /var/lib/mongodb1 && /var/lib/mongodb2 && /var/lib/mongodb3`

 and then you have to set the replica : 

`sudo mongod --port 27018 --dbpath /var/lib/mongodb1 --replSet rs0`


`sudo mongod --port 27019 --dbpath /var/lib/mongodb2 --replSet rs0`


`sudo mongod --port 27020 --dbpath /var/lib/mongodb3 --replSet rs0`



After you should to connect the secondary to the primary.
For this connect to the first : `mongo --port 27018` when you are connected, run this `rs.initiate()`


Then add the secondaries : `rs.add("localhost:2019")` && `rs.add("localhost:2020")`


| WARNING:If you modify something in replica set name or other things, you need to change the local uri in config/config.js |
| --- |


Clone this repository to your local storage:
`git clone https://eamarelo@bitbucket.org/eamarelo/opti-project.git`


 and install dependencies:
`npm i`


if you want to run the project to http://localhost:3000/  in local database:
`npm run local`


#### Run test

Clone this repository to your local storage:
`git clone https://eamarelo@bitbucket.org/eamarelo/opti-project.git`


 and install dependencies:
`npm i`


You need to install mocha globally : `npm i -g mocha`

To run Unit test & intregation test you have to run this cli : `npm run test`


#### Table Characters

| Character | type
| --- | --- 
| _id | ObjectId
|Name | String
| Race | String
| Type | String
| date | Date()

## REPLICA SET SCHEMA

![Alt text](./diagram-opti.png "Title")


### USER PART
#### Create a character

URI : http://localhost:3000/api/auth/register
method: POST  
Data to send :
`{
    "Name": "Solene",
    "Age": 22,
    "Race": "Elf de niveau 1"
}`
  
response: 
`{
    "_id": "5d09e868e71fe011e9863f00",
    "Name": "Solene",
    "Age": 22,
    "Race": "Elf de niveau 1",
    "createdOn": "2019-06-19T07:46:48.493Z",
    "__v": 0
}`
  
#### Get list all character
  
URI : http://localhost:3000/characters/  
method: GET
  
response: 
`[
    {
        "_id": "5d09e868e71fe011e9863f00",
        "Name": "Solene",
        "Age": 22,
        "Race": "Elf de niveau 1",
        "createdOn": "2019-06-19T07:46:48.493Z",
        "__v": 0
    },
    {
        "_id": "5d09e868e71fe011e8421f00",
        "Name": "Jp",
        "Age": 26,
        "Race": "Géant",
        "createdOn": "2019-06-19T07:46:48.493Z",
        "__v": 0
    }]`
#### Get character by Id

URI : http://localhost:3000/characters/:id
method: GET

response: 
`{
    "_id": "5d09e868e71fe011e9863f00",
    "Name": "Solene",
    "Age": 22,
    "Race": "Elf de niveau 1",
    "createdOn": "2019-06-19T07:46:48.493Z",
    "__v": 0
}`

#### Update an character

URI : http://localhost:3000/characters/:id
method: PUT 
data to send :
`{
	"Name": "Aladin"
}`
response before modification: 
`{
    "_id": "5d09e868e71fe011e9863f00",
    "Name": "Aladin",
    "Age": 22,
    "Race": "Elf de niveau 1",
    "createdOn": "2019-06-19T07:46:48.493Z",
    "__v": 0
}`

response before modification: `{
    "_id": "5d09e868e71fe011e9863f00",
    "Name": "Solene",
    "Age": 22,
    "Race": "Elf de niveau 1",
    "createdOn": "2019-06-19T07:46:48.493Z",
    "__v": 0
}`

#### Delete an character

URI : http://localhost:3000/characters/:id
method : DELETE
resonse: User was deleted.

