const mongoose = require("mongoose");

let dbURI = "mongodb://mongo-rs01,mongo-rs02,mongo-rs03/project?replicaSet=rs0"

const options = {
  useNewUrlParser: true,
  useFindAndModify: false,
  reconnectTries: Number.MAX_VALUE,
  poolSize: 10
};

mongoose.connect(dbURI, options).then(
  () => {
    console.log("Database connection established!");
  },
  err => {
    console.log("Error connecting Database instance due to: ", err);
  }
);

// require any models

require("../models/Character");