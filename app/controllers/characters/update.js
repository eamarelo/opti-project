const Character = require("../../models/Character");

exports.updateCharacter = (req, res) => {
  Character.findOneAndUpdate(
    { _id: req.params._id },
    req.body,
    { new: true },
    (err, character) => {
      if (err) {
        res.status(500).send(err);
      }
      res.status(200).json(character);
    }
  );
};