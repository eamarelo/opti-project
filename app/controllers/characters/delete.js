const Character = require("../../models/Character");

exports.deleteCharacter = (req, res) => {
  Character.deleteOne({ _id: req.params._id }, (err, character) => {
    if (err) {
      res.status(404).send(err);
    }
    res.status(200).json({ message: "Character deleted" });
  });
};