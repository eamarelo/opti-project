const Character = require("../../models/Character");

exports.listCharacters = (req, res) => {
  Character.find({}, (err, characters) => {
    if (err) {
      res.status(500).send(err);
    }
    res.status(200).json(characters);
  });
};