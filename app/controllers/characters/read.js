const Character = require("../../models/Character");

exports.readCharacter = (req, res) => {
  Character.findById(req.params._id, (err, character) => {
    if (err) {
      res.status(500).send(err);
    }
    res.status(200).json(character);
  });
};