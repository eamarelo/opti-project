require("./config/config");
const createCharacterController = require("./controllers/characters/create");
const listCharacterController = require("./controllers/characters/get");
const readCharacterController = require("./controllers/characters/read");
const updateCharacterController = require("./controllers/characters/update");
const deleteCharacterController = require("./controllers/characters/delete");

const bodyParser = require("body-parser");

const express = require("express");

const app = express();

const port = process.env.PORT || 3000;

app.use(bodyParser.urlencoded({ extended: true }));
app.use(bodyParser.json());

app
  .route("/characters")
  .get(listCharacterController.listCharacters)
  .post(createCharacterController.createNewCharacter);

app
  .route("/characters/:_id")
  .get(readCharacterController.readCharacter)
  .put(updateCharacterController.updateCharacter)
  .delete(deleteCharacterController.deleteCharacter);

app.listen(port, () => {
  console.log(`Server running at http://localhost:${port}`);
});