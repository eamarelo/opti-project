const mongoose = require("mongoose");
const Schema = mongoose.Schema;

const CharactersSchema = new Schema({
  Name: {
    type: String,
    required: true
  },
  Age: {
    type: Number,
    required: true
  },
  Race: {
    type: String,
    required: true
  },
  createdOn: {
    type: Date,
    default: Date.now
  }
});

module.exports = mongoose.model("Character", CharactersSchema);