const Character = require("../../models/Character");

exports.createNewCharacter = (req, res) => {
	console.log(req.body)
  // let newTask = new Task(req.body);
     let newCharacter = new Character({ Name: req.body.Name, Age: req.body.Age, Race: req.body.Race });

  newCharacter.save((err, character) => {
    if (err) {
      res.status(500).send(err);
    }
    res.status(201).json(character);
  });
};