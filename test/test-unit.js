const mongoose = require("mongoose");
var expect    = require("chai").expect;
var Character = require('../models/Character');
let chai = require('chai');
let chaiHttp = require('chai-http');
let server = require('../index.js');
let should = chai.should();

chai.use(chaiHttp);

 describe('test connection', () => {
    it('test connection', (done) => {

        //You can modify Uri tho the local uri to test the local db.
        mongoose.connect('mongodb+srv://admin-opti:password-opti@cluster0-hvp9t.mongodb.net/test?retryWrites=true&w=majority', { useNewUrlParser: true });
        const db = mongoose.connection;
        db.on('error', console.error.bind(console, 'connection error'));
        db.once('open', function() {
          done();
        });
    });
});

describe('check request CRUD characters', function() {
	before(function (done) {
	  mongoose.connect('mongodb+srv://admin-opti:password-opti@cluster0-hvp9t.mongodb.net/test?retryWrites=true&w=majority', { useNewUrlParser: true });
	  const db = mongoose.connection;
	  db.on('error', console.error.bind(console, 'connection error'));
	  db.once('open', function() {
	    console.log('------ The connection is etablished -----');
	    done();
	  });
	});

      describe('/GET characters', () => {
      it('it should GET all the characters', (done) => {
        chai.request('localhost:3000')
            .get('/characters')
            .end((err, res) => {
                  res.should.have.status(200);
              done();
            });
      });
  });

      describe('/POST characters', () => {
          it('Post character', (done) => {
              let character = {
                  Name: "Arnaud",
                  Age: 10,
                  Race: "Grand schroumpf"
              }
            chai.request("localhost:3000")
                .post('/characters')
                .send(character)
                .end((err, res) => {
                      res.should.have.status(201);
                  done();
                });
          });

      });

      describe('/POST  HTTP 500 characters', () => {
          it('No post character status 500 -error', (done) => {
              let character = {
                  Name: "Arnaud",
                  Age: 10
              }
            chai.request("localhost:3000")
                .post('/characters')
                .send(character)
                .end((err, res) => {
                      res.should.have.status(500);
                  done();
                });
          });

      });

      describe('/GET/:id character', () => {
          it('it should GET a character by the given id', (done) => {
              let character = new Character({ Name: "Sulleyman", Age: 120, Race: "Paladin bourrin" });
              character.save((err, character) => {
                  chai.request("localhost:3000")
                .get('/characters/' + character.id)
                .send(character)
                .end((err, res) => {
                      res.should.have.status(200);
                      res.body.should.be.a('object');
                      res.body.should.have.property('Name');
                      res.body.should.have.property('Age');
                      res.body.should.have.property('Race');
                      res.body.should.have.property('_id').eql(character.id);
                  done();
                });
              });

          });
         });

      describe('/PUT/:id characters', () => {
          it('it should UPDATE a character given the id', (done) => {
              let character = new Character({ Name: "Nathan", Age: 18, Race: "Nathan n'attend pas" })
              character.save((err, character) => {
                    chai.request("localhost:3000")
                    .put('/characters/' + character.id)
                    .send({ Name: "Sercan" })
                    .end((err, res) => {
                          res.should.have.status(200);
                          res.body.should.be.a('object');
                      done();
                    });
              });
          });
      });

      describe('/DELETE/:id character', () => {
          it('it should DELETE a character given the id', (done) => {
              let character = new Character({ Name: "Clement", Age: 21, Race: "Narnian" })
              character.save((err, character) => {
                    chai.request("localhost:3000")
                    .delete('/characters/' + character.id)
                    .end((err, res) => {
                          res.should.have.status(200);
                          res.body.should.be.a('object');
                          res.body.should.have.property('message').eql('Character deleted');
                      done();
                    });
              });
          });
      });
	
    after(function(done){
      mongoose.connection.db.dropDatabase(function(){
        mongoose.connection.close(done);
      });
    });
});

describe('check if Name is required', function() {
    it('should be invalid if Name is empty', function(done) {
        var m = new Character();
 
        m.validate(function(err) {
            expect(err.errors.Name).to.exist;
            done();
        });
    });
});

describe('check if Age is required', function() {
    it('should be invalid if Age is empty', function(done) {
        var m = new Character();
 
        m.validate(function(err) {
            expect(err.errors.Age).to.exist;
            done();
        });
    });
});

describe('check if Race is required', function() {
    it('should be invalid if Race is empty', function(done) {
        var m = new Character();
 
        m.validate(function(err) {
            expect(err.errors.Race).to.exist;
            done();
        });
    });
});
